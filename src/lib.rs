#![no_std]
#![feature(impl_trait_in_assoc_type)]
#![feature(never_type)]

extern crate alloc;

pub mod clock;
pub mod heap;
pub mod lamp;
pub mod never;
pub mod ntp;
pub mod nvs;
pub mod state;
pub mod web;
