use alloc::boxed::Box;
use core::any::Any;

#[inline]
pub fn make_static<'a, T>(x: T) -> &'a mut T {
    make_static_with(|| x)
}

pub fn make_static_with<'a, F, T>(f: F) -> &'a mut T
where
    F: FnOnce() -> T,
{
    let x = Box::new_uninit();
    let x = Box::leak(x);
    x.write(f());
    unsafe { x.assume_init_mut() }
}

#[inline]
pub fn make_any_static<'a, T: 'static>(x: T) -> &'a mut dyn Any {
    make_static(x)
}

#[inline]
pub fn make_any_static_with<'a, F, T: 'static>(f: F) -> &'a mut dyn Any
where
    F: FnOnce() -> T,
{
    make_static_with(f)
}
