#![no_std]
#![no_main]
#![feature(impl_trait_in_assoc_type)]

use esp_alloc as _;
use esp_backtrace as _;
use esp_println as _;

use chrono::{NaiveDateTime, NaiveTime, Timelike};
use core::any::Any;
use defmt::{debug, trace, unreachable, unwrap, Format};
use embassy_executor::task;
use embassy_futures::select::{select, Either};
use embassy_net::{
    Config as NetworkConfig, DhcpConfig, IpEndpoint, Ipv4Address, Ipv4Cidr, Runner as WifiRunner, Stack as WifiStack,
    StackResources as WifiStackResources, StaticConfigV4,
};
use embassy_sync::{
    blocking_mutex::raw::NoopRawMutex,
    channel::{Channel, DynamicSender, Receiver},
    mutex::Mutex,
    signal::Signal,
};
use embassy_time::{Duration, Instant, Ticker, Timer};
use esp_hal::{
    clock::CpuClock,
    config::{WatchdogConfig, WatchdogStatus},
    gpio::{Input, Pull},
    ledc::Ledc,
    main,
    reset::{software_reset, SleepSource},
    rng::Rng,
    rtc_cntl::{reset_reason, wakeup_cause, Rtc, Rwdt, SocResetReason},
    time::ExtU64,
    timer::{timg::TimerGroup, AnyTimer},
    Cpu,
};
use esp_hal_embassy::Executor;
use esp_wifi::{
    config::PowerSaveMode,
    wifi::{
        wifi_state, AccessPointConfiguration, AuthMethod, ClientConfiguration, Protocol, WifiApDevice, WifiController,
        WifiDevice, WifiEvent, WifiMode, WifiStaDevice, WifiState,
    },
};
use heapless::{String, Vec};
use huomenta::{
    clock::Clock,
    heap,
    lamp::Lamp,
    never,
    never::NeverScope,
    ntp::NtpClient,
    nvs::{Nvs, Slot},
    state,
    web::{Action, Event, WebServer},
};
use serde::{Deserialize, Serialize};

#[derive(Format, Serialize, Deserialize)]
struct Settings {
    wifi_ssid: String<32>,
    wifi_password: String<64>,
    wake_up: NaiveTime,
}

const HOSTNAME: &str = env!("CARGO_PKG_NAME");

#[main]
fn main() -> ! {
    huomenta::never::call(run)
}

fn run(scope: &NeverScope) -> ! {
    match reset_reason(Cpu::current()) {
        Some(SocResetReason::ChipPowerOn) => debug!("Power on reset"), // Cold boot
        Some(SocResetReason::CoreSw) => debug!("Software reset digital core"),
        Some(SocResetReason::CoreDeepSleep) => {
            debug!("Deep sleep reset digital core");
            match wakeup_cause() {
                SleepSource::Undefined => debug!("Undefined wakeup cause"),
                SleepSource::Ext0 => debug!("Wakeup caused by external signal using RTC_IO"),
                SleepSource::Ext1 => debug!("Wakeup caused by external signal using RTC_CNTL"),
                SleepSource::Timer => debug!("Wakeup caused by timer"),
                SleepSource::TouchPad => debug!("Wakeup caused by touchpad"),
                SleepSource::Ulp => debug!("Wakeup caused by ULP program"),
                SleepSource::Cocpu => debug!("Wakeup caused by co-processor interrupt"),
                SleepSource::CocpuTrapTrig => debug!("Wakeup caused by co-processor trap"),
                _ => unreachable!(),
            }
        }
        Some(SocResetReason::CoreSdio) => debug!("SDIO module reset digital core"),
        Some(SocResetReason::CoreMwdt0) => debug!("Timer Group 0 Watch dog reset digital core"),
        Some(SocResetReason::CoreMwdt1) => debug!("Timer Group 1 Watch dog reset digital core"),
        Some(SocResetReason::CoreRtcWdt) => debug!("RTC watchdog reset digital core"),
        Some(SocResetReason::CpuMwdt0) => debug!("Timer Group Watch dog reset CPU"),
        Some(SocResetReason::Cpu0Sw) => debug!("Software reset CPU"),
        Some(SocResetReason::Cpu0RtcWdt) => debug!("RTC watchdog reset CPU"),
        Some(SocResetReason::SysBrownOut) => debug!("Brownout reset"),
        Some(SocResetReason::SysRtcWdt) => debug!("RTC watchdog reset digital core and RTC module"),
        _ => debug!("Unknown reset reason"),
    }

    let config = esp_hal::Config::default()
        .with_cpu_clock(CpuClock::max())
        .with_watchdog(WatchdogConfig::default().with_rwdt(WatchdogStatus::Enabled(1.minutes())));
    let peripherals = esp_hal::init(config);

    esp_alloc::heap_allocator!(64 * 1024);

    let nvs = unwrap!(Nvs::take());
    never!(scope, nvs);

    let (mode, settings) = {
        nvs.load::<Settings>(Slot::A)
            .map(|settings| {
                debug!("Settings loaded");
                (WifiMode::Sta, settings)
            })
            .unwrap_or_else(|| {
                debug!("No settings found");
                let settings = Settings {
                    wifi_ssid: unwrap!(HOSTNAME.try_into()),
                    wifi_password: String::new(),
                    wake_up: unwrap!(NaiveTime::from_hms_opt(6, 30, 0)),
                };
                (WifiMode::Ap, settings)
            })
    };

    let rtc = Rtc::new(peripherals.LPWR);
    never!(scope, rtc);
    debug!("RTC initialized");

    let i2c = peripherals.I2C0.into();
    let sda = peripherals.GPIO21.into();
    let scl = peripherals.GPIO22.into();
    let mut clock = Clock::new(i2c, sda, scl);
    clock.set_monthly_alarm(1, 18, 30);
    clock.set_daily_alarm(settings.wake_up.hour(), settings.wake_up.minute());
    let clock = Mutex::new(clock);
    never!(scope, clock);
    debug!("Clock initialized");

    let wake_up = Signal::new();
    never!(scope, wake_up);

    let stop = Signal::new();
    never!(scope, stop);

    let timg1 = TimerGroup::new(peripherals.TIMG1);
    let timer0: AnyTimer = timg1.timer0.into();
    let timer1: AnyTimer = timg1.timer1.into();
    esp_hal_embassy::init([timer0, timer1]);
    debug!("Embassy initialized");

    let actions = Channel::new();
    never!(scope, actions);
    let events = Channel::new();
    never!(scope, events);
    let adjust = Signal::new();
    never!(scope, adjust);
    let time = Signal::new();
    never!(scope, time);

    if state::init(rtc) {
        debug!("First boot");
        adjust.signal(());
    }

    let ledc = Ledc::new(peripherals.LEDC);
    never!(scope, ledc);
    let pwm_pin = peripherals.GPIO32.into();
    let psu_pin = peripherals.GPIO33.into();
    let lamp = Mutex::new(Lamp::new(ledc, pwm_pin, psu_pin));
    never!(scope, lamp);
    debug!("Lamp driver initialized");

    let mut rng = Rng::new(peripherals.RNG);
    let seed = (rng.random() as u64) << 32 | rng.random() as u64;

    let init = {
        let timg0 = TimerGroup::new(peripherals.TIMG0);
        unwrap!(esp_wifi::init(timg0.timer0, rng, peripherals.RADIO_CLK))
    };
    never!(scope, init);
    debug!("WiFi hardware initialized");

    let resources = WifiStackResources::<16>::new();
    never!(scope, resources);

    let ssid = settings.wifi_ssid.clone();
    let password = settings.wifi_password.clone();

    let (runner, controller, stack) = match mode {
        WifiMode::Sta => {
            let config = ClientConfiguration {
                ssid,
                bssid: None,
                auth_method: AuthMethod::WPA2Personal,
                password,
                channel: None,
            };
            let (device, controller): (WifiDevice<'static, WifiStaDevice>, _) =
                unwrap!(esp_wifi::wifi::new_with_config(init, peripherals.WIFI, config));
            debug!("WiFi client initialized");

            let mut config = DhcpConfig::default();
            config.hostname = HOSTNAME.try_into().ok();
            let config = NetworkConfig::dhcpv4(DhcpConfig::default());

            let (stack, runner) = embassy_net::new(device, config, resources, seed);
            debug!("Embassy network initialized");

            (heap::make_any_static(runner), controller, stack)
        }
        WifiMode::Ap => {
            let config = AccessPointConfiguration {
                ssid,
                ssid_hidden: false,
                channel: 1,
                secondary_channel: None,
                protocols: Protocol::P802D11B | Protocol::P802D11BG | Protocol::P802D11BGN,
                auth_method: AuthMethod::None,
                password,
                max_connections: 4,
            };
            let (device, controller): (WifiDevice<'static, WifiApDevice>, _) =
                unwrap!(esp_wifi::wifi::new_with_config(init, peripherals.WIFI, config));
            debug!("WiFi access point initialized");

            let config = StaticConfigV4 {
                address: Ipv4Cidr::new(Ipv4Address::new(192, 168, 4, 1), 24),
                gateway: None,
                dns_servers: Vec::new(),
            };
            let config = NetworkConfig::ipv4_static(config);

            let (stack, runner) = embassy_net::new(device, config, resources, seed);
            debug!("Embassy network initialized");

            (heap::make_any_static(runner), controller, stack)
        }
        _ => unreachable!(),
    };

    let alarm = Input::new(peripherals.GPIO36, Pull::Up);
    never!(scope, alarm);

    let executor = Executor::new();
    never!(scope, executor);
    executor.run(|spawner| {
        for id in 0..TASK_WEB_POOL_SIZE {
            unwrap!(spawner.spawn(task_web_server(id, stack, actions.dyn_sender(), events.dyn_sender())));
        }
        unwrap!(spawner.spawn(task_wifi_runner(mode, runner)));
        unwrap!(spawner.spawn(task_wifi_controller(controller)));
        unwrap!(spawner.spawn(task_events(events.receiver(), nvs, settings)));
        unwrap!(spawner.spawn(task_watchdog()));
        unwrap!(spawner.spawn(task_alarm(alarm, wake_up, adjust, clock)));
        unwrap!(spawner.spawn(task_rtc(clock, rtc, time)));
        unwrap!(spawner.spawn(task_actions(actions.receiver(), lamp, clock, stop)));

        if matches!(mode, WifiMode::Sta) {
            unwrap!(spawner.spawn(task_ntp(stack, rtc, adjust, time)));
            unwrap!(spawner.spawn(task_scheduler(lamp, wake_up, stop)));
        }
    })
}

#[task]
async fn task_watchdog() -> ! {
    let mut rwtd = Rwdt::new();
    loop {
        Timer::after_secs(50).await;
        trace!("Feeding the watchdog");
        rwtd.feed();
    }
}

#[task]
async fn task_alarm(
    alarm: &'static mut Input<'static>,         // Alarm pin
    wake_up: &'static Signal<NoopRawMutex, ()>, // Signal to wake up
    adjust: &'static Signal<NoopRawMutex, ()>,  // Signal to adjust the time
    clock: &'static Mutex<NoopRawMutex, Clock>, // External RTC
) -> ! {
    loop {
        alarm.wait_for_low().await;
        debug!("Alarm interrupt triggered");

        let mut clock = clock.lock().await;
        if clock.take_daily_alarm() {
            debug!("Daily alarm matched");
            wake_up.signal(());
            continue;
        }
        if clock.take_monthly_alarm() {
            debug!("Monthly alarm matched");
            adjust.signal(());
            continue;
        }
        debug!("No alarm matched");
    }
}

#[task]
async fn task_rtc(
    clock: &'static Mutex<NoopRawMutex, Clock>, // External RTC
    rtc: &'static Rtc<'static>,                 // Internal RTC
    time: &'static Signal<NoopRawMutex, NaiveDateTime>,
) -> ! {
    // The internal RTC has rather poor accuracy and drifts quickly, so we keep it in sync with the
    // external RTC every so often. This is cheaper than using NTP, which we do less frequently.
    let mut ticker = Ticker::every(Duration::from_secs(990));
    // Make the ticker fire immediately
    ticker.reset_at(Instant::MIN);
    loop {
        match select(ticker.next(), time.wait()).await {
            Either::First(_) => {
                let now = clock.lock().await.current_time();
                debug!("Updating internal RTC to {}", now);
                rtc.set_current_time(now);
            }
            Either::Second(now) => {
                debug!("Updating external RTC to {}", now);
                clock.lock().await.set_current_time(now);
            }
        }
    }
}

#[task]
async fn task_ntp(
    stack: WifiStack<'static>,
    rtc: &'static Rtc<'static>,                // Internal RTC
    adjust: &'static Signal<NoopRawMutex, ()>, // Signal to adjust the time
    time: &'static Signal<NoopRawMutex, NaiveDateTime>,
) -> ! {
    let mut client = unwrap!(NtpClient::new(stack, rtc));
    stack.wait_config_up().await;

    let config = unwrap!(stack.config_v4());
    let gateway = unwrap!(config.gateway);
    let gateway = IpEndpoint::new(gateway.into(), 123);
    debug!("IP address is {}", config.address);

    loop {
        adjust.wait().await;
        debug!("Adjusting time");

        // Assume UTC time from the RTC
        let origin = rtc.current_time().and_utc();

        debug!("Querying NTP server");
        let now = unwrap!(client.query(origin, gateway).await);
        let now = now.naive_utc();
        debug!("Date is {}", now);

        rtc.set_current_time(now);
        time.signal(now);
        debug!("Time adjusted");
    }
}

const TASK_WEB_POOL_SIZE: usize = 4;

#[task(pool_size = TASK_WEB_POOL_SIZE)]
async fn task_web_server(
    id: usize,
    stack: WifiStack<'static>,
    actions: DynamicSender<'static, Action>, // Channel to send actions
    events: DynamicSender<'static, Event>,   // Channel to send events
) -> ! {
    let mut server = WebServer::new(stack, actions, events);
    stack.wait_config_up().await;
    let mut tcp_rx_buffer = [0; 1024];
    let mut tcp_tx_buffer = [0; 1024];
    let mut http_buffer = [0; 2048];
    debug!("Web server {} running", id);
    server.run(id, &mut tcp_rx_buffer, &mut tcp_tx_buffer, &mut http_buffer).await
}

#[task]
async fn task_scheduler(
    lamp: &'static Mutex<NoopRawMutex, Lamp>,   // Lamp driver
    wake_up: &'static Signal<NoopRawMutex, ()>, // Signal to wake up
    stop: &'static Signal<NoopRawMutex, ()>,    // Signal to stop
) -> ! {
    // TODO make these configurable
    const DURATION: u32 = 30 * 60;
    const STEPS: u32 = 600;
    loop {
        wake_up.wait().await;
        debug!("Waking up");

        let duration = (DURATION / STEPS).into();
        let mut ticker = Ticker::every(Duration::from_secs(duration));

        for step in 0..STEPS {
            let brightness = step as f32 / (STEPS - 1) as f32;
            lamp.lock().await.set_brightness(brightness);

            if matches!(select(stop.wait(), ticker.next()).await, Either::First(_)) {
                debug!("Stopping now");
                continue;
            }
        }

        debug!("Peak brightness");
        stop.wait().await;
        debug!("Stopping");
        lamp.lock().await.set_brightness(0.0);
    }
}

#[task]
async fn task_events(
    events: Receiver<'static, NoopRawMutex, Event, TASK_WEB_POOL_SIZE>, // Channel to receive events
    nvs: &'static mut Nvs,                                              // Non-volatile storage
    mut settings: Settings,
) -> ! {
    loop {
        debug!("Waiting for an event");
        match events.receive().await {
            Event::Network { wifi_ssid, wifi_password } => {
                debug!("Saving network settings");
                settings.wifi_ssid = wifi_ssid;
                settings.wifi_password = wifi_password;
                nvs.save(Slot::A, &settings);
            }
            Event::WakeUpTime(time) => {
                debug!("Saving wake-up time");
                settings.wake_up = time;
                nvs.save(Slot::A, &settings);
            }
            Event::Reset => {
                debug!("Resetting settings");
                nvs.erase(Slot::A);
            }
        }
    }
}

#[task]
async fn task_actions(
    actions: Receiver<'static, NoopRawMutex, Action, TASK_WEB_POOL_SIZE>, // Channel to receive actions
    lamp: &'static Mutex<NoopRawMutex, Lamp>,                             // Lamp driver
    clock: &'static Mutex<NoopRawMutex, Clock>,                           // External RTC
    stop: &'static Signal<NoopRawMutex, ()>,                              // Signal to stop
) -> ! {
    loop {
        debug!("Waiting for an action");
        match actions.receive().await {
            Action::SetBrightness(brightness) => {
                if brightness == 0 {
                    debug!("Stopping");
                    stop.signal(());
                } else {
                    debug!("Setting brightness to {}%", brightness);
                    let brightness = (brightness as f32) / 100.0;
                    lamp.lock().await.set_brightness(brightness);
                }
            }
            Action::SetWakeUpTime(time) => {
                debug!("Setting wake-up time to {}", time);
                // Update wake-up time on the external RTC
                let mut clock = clock.lock().await;
                clock.set_daily_alarm(time.hour(), time.minute());
            }
            Action::Restart => {
                debug!("Device reset in 5 seconds");
                Timer::after_secs(5).await;
                software_reset();
            }
        }
    }
}

#[task]
async fn task_wifi_controller(mut controller: WifiController<'static>) -> ! {
    unwrap!(controller.set_power_saving(PowerSaveMode::None));
    loop {
        Timer::after_secs(1).await;
        match wifi_state() {
            WifiState::StaConnected => {
                controller.wait_for_event(WifiEvent::StaDisconnected).await;
                debug!("WiFi station disconnected");
            }
            WifiState::ApStarted => {
                controller.wait_for_event(WifiEvent::ApStop).await;
                debug!("WiFi access point stopped");
            }
            _ => {}
        }
        if !matches!(controller.is_started(), Ok(true)) {
            unwrap!(controller.start_async().await);
        }
        if matches!(controller.is_sta_enabled(), Ok(true)) {
            match controller.connect_async().await {
                Ok(_) => debug!("WiFi connected!"),
                Err(_) => debug!("Failed to connect to WiFi"),
            }
        }
    }
}

#[task]
async fn task_wifi_runner(mode: WifiMode, runner: &'static mut dyn Any) -> ! {
    match mode {
        WifiMode::Sta => {
            let runner = unwrap!(runner.downcast_mut::<WifiRunner<'static, WifiDevice<'static, WifiStaDevice>>>());
            debug!("WiFi running in station mode");
            runner.run().await
        }
        WifiMode::Ap => {
            let runner = unwrap!(runner.downcast_mut::<WifiRunner<'static, WifiDevice<'static, WifiApDevice>>>());
            debug!("WiFi running in access point mode");
            runner.run().await
        }
        _ => unreachable!(),
    }
}
