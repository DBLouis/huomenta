//! Non-volatile storage (NVS) module.
use core::{cell::Cell, marker::PhantomData};
use defmt::{assert, debug, unwrap, write, Format, Formatter};
use esp_hal::rom::crc::crc32_be;
use esp_storage::ll::*;
use portable_atomic::{AtomicBool, Ordering};
use serde::{Deserialize, Serialize};

const NVS_OFFSET: u32 = 0x9000;
const NVS_SIZE: u32 = 0x6000; // 24KB

const SECTOR_SIZE: u32 = 4096;

static LOCKED: AtomicBool = AtomicBool::new(true);

#[derive(Format)]
pub struct Nvs {
    // Word-aligned buffer for reading and writing.
    buffer: [u32; SECTOR_SIZE as usize / 4],
    _not_sync: PhantomData<Cell<()>>,
}

impl Nvs {
    const fn new() -> Self {
        let buffer = [0; SECTOR_SIZE as usize / 4];
        Self { buffer, _not_sync: PhantomData }
    }

    pub fn take() -> Option<Self> {
        if LOCKED.compare_exchange(true, false, Ordering::Acquire, Ordering::Relaxed).is_err() {
            return None;
        }
        flash_unlock();
        debug!("Flash unlocked");
        Some(Self::new())
    }

    pub fn load<T>(&mut self, slot: Slot) -> Option<T>
    where
        T: for<'de> Deserialize<'de>,
    {
        debug!("Read slot {}", slot);
        flash_read(slot, &mut self.buffer);
        let (crc, data) = unwrap!(self.buffer.split_first());
        debug!("CRC={:x}", crc);
        let data = bytemuck::must_cast_slice(data);
        if crc32_be(!0, data) == *crc {
            let Ok(value) = postcard::from_bytes(data) else {
                debug!("Deserialization failed");
                return None;
            };
            Some(value)
        } else {
            debug!("CRC mismatch");
            None
        }
    }

    pub fn save<T>(&mut self, slot: Slot, value: &T)
    where
        T: Serialize + ?Sized,
    {
        let (crc, data) = unwrap!(self.buffer.split_first_mut());
        let data = bytemuck::must_cast_slice_mut(data);
        let _ = unwrap!(postcard::to_slice(value, data));
        *crc = crc32_be(!0, data);
        debug!("CRC={:x}", *crc);
        debug!("Erase slot {}", slot);
        flash_erase(slot);
        debug!("Write slot {}", slot);
        flash_write(slot, &self.buffer);
    }

    pub fn erase(&mut self, slot: Slot) {
        debug!("Erase slot {}", slot);
        flash_erase(slot);
    }
}

#[inline(never)]
#[link_section = ".rwtext"]
fn flash_unlock() {
    let result = unsafe { spiflash_unlock() };
    assert!(result.is_ok());
}

#[inline(never)]
#[link_section = ".rwtext"]
fn flash_read(slot: Slot, buffer: &mut [u32]) {
    let offset = slot.offset();
    let len = buffer.len() as u32 * 4;
    let ptr = buffer.as_mut_ptr();
    let result = unsafe { spiflash_read(offset, ptr, len) };
    assert!(result.is_ok());
}

#[inline(never)]
#[link_section = ".rwtext"]
fn flash_erase(slot: Slot) {
    let result = unsafe { spiflash_erase_sector(slot.sector()) };
    assert!(result.is_ok());
}

#[inline(never)]
#[link_section = ".rwtext"]
fn flash_write(slot: Slot, buffer: &[u32]) {
    let offset = slot.offset();
    let len = buffer.len() as u32 * 4;
    let ptr = buffer.as_ptr();
    let result = unsafe { spiflash_write(offset, ptr, len) };
    assert!(result.is_ok());
}

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct Slot {
    index: u32,
}

impl Slot {
    pub const A: Slot = Slot::new(0);
    pub const B: Slot = Slot::new(1);
    pub const C: Slot = Slot::new(2);
    pub const D: Slot = Slot::new(3);
    pub const E: Slot = Slot::new(4);
    pub const F: Slot = Slot::new(5);

    const fn new(index: u32) -> Self {
        core::assert!(index < NVS_SIZE / SECTOR_SIZE);
        Self { index }
    }

    pub const fn offset(&self) -> u32 {
        NVS_OFFSET + self.index * SECTOR_SIZE
    }

    pub const fn size(&self) -> u32 {
        SECTOR_SIZE
    }

    pub const fn sector(&self) -> u32 {
        NVS_OFFSET / SECTOR_SIZE + self.index
    }
}

impl Format for Slot {
    fn format(&self, f: Formatter<'_>) {
        write!(f, "{}", (b'A' + self.index as u8) as char);
    }
}
