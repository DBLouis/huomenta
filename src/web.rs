use crate::{heap, state};
use chrono::{FixedOffset, NaiveTime};
use defmt::{debug, unwrap, Format};
use embassy_net::Stack;
use embassy_sync::channel::DynamicSender;
use embassy_time::Duration;
use heapless::String;
use picoserve::{
    extract::{Form, Json},
    listen_and_serve,
    response::{fs::File, status::StatusCode},
    routing, AppBuilder, AppRouter, Config, Router, Timeouts,
};
use serde::Deserialize;

const INDEX: &str = include_str!("index.html");
const SCRIPT: &str = include_str!("script.js");
const BOOTSTRAP_CSS: &str = include_str!("bootstrap.min.css");
const BOOTSTRAP_JS: &str = include_str!("bootstrap.bundle.min.js");
const FAVICON: &[u8] = include_bytes!("favicon/favicon.ico");
const FAVICON_16: &[u8] = include_bytes!("favicon/favicon-16x16.png");
const FAVICON_32: &[u8] = include_bytes!("favicon/favicon-32x32.png");
const FAVICON_APPLE_TOUCH: &[u8] = include_bytes!("favicon/apple-touch-icon.png");
const FAVICON_ANDROID_192: &[u8] = include_bytes!("favicon/android-chrome-192x192.png");
const FAVICON_ANDROID_512: &[u8] = include_bytes!("favicon/android-chrome-512x512.png");
const FAVICON_MANIFEST: &[u8] = include_bytes!("favicon/site.webmanifest");

#[derive(Format, Deserialize)]
struct NetworkForm {
    wifi_ssid: String<32>,
    wifi_password: String<64>,
}

#[derive(Format, Deserialize)]
struct TimeForm {
    wake_up: NaiveTime,
    timezone: i32,
}

#[derive(Format, Deserialize)]
struct BrightnessForm {
    brightness: u32,
}

#[derive(Format, Clone)]
pub enum Action {
    SetBrightness(u32),
    SetWakeUpTime(NaiveTime),
    Restart,
}

#[derive(Format, Clone)]
pub enum Event {
    Network { wifi_ssid: String<32>, wifi_password: String<64> },
    WakeUpTime(NaiveTime),
    Reset,
}

struct AppBuilderImpl {
    actions: DynamicSender<'static, Action>,
    events: DynamicSender<'static, Event>,
}

impl AppBuilder for AppBuilderImpl {
    type PathRouter = impl routing::PathRouter;

    fn build_app(self) -> Router<Self::PathRouter> {
        let Self { actions, events } = self;
        Router::new()
            .route("/", routing::get_service(File::html(INDEX)))
            .route("/script.js", routing::get_service(File::javascript(SCRIPT)))
            .route("/bootstrap.min.css", routing::get_service(File::css(BOOTSTRAP_CSS)))
            .route("/bootstrap.bundle.min.js", routing::get_service(File::javascript(BOOTSTRAP_JS)))
            .route("/favicon.ico", routing::get_service(File::with_content_type("image/x-icon", FAVICON)))
            .route("/favicon-16x16.png", routing::get_service(File::with_content_type("image/png", FAVICON_16)))
            .route("/favicon-32x32.png", routing::get_service(File::with_content_type("image/png", FAVICON_32)))
            .route(
                "/apple-touch-icon.png",
                routing::get_service(File::with_content_type("image/png", FAVICON_APPLE_TOUCH)),
            )
            .route(
                "/android-chrome-192x192.png",
                routing::get_service(File::with_content_type("image/png", FAVICON_ANDROID_192)),
            )
            .route(
                "/android-chrome-512x512.png",
                routing::get_service(File::with_content_type("image/png", FAVICON_ANDROID_512)),
            )
            .route(
                "/site.webmanifest",
                routing::get_service(File::with_content_type("application/manifest+json", FAVICON_MANIFEST)),
            )
            .route(
                "/brightness",
                routing::post(move |form: Form<BrightnessForm>| {
                    debug!("{}", &form.0);
                    let BrightnessForm { brightness } = form.0;
                    async move {
                        actions.send(Action::SetBrightness(brightness)).await;
                        StatusCode::NO_CONTENT
                    }
                }),
            )
            .route(
                "/time",
                routing::post(move |form: Form<TimeForm>| {
                    debug!("{}", &form.0);
                    let TimeForm { wake_up, timezone } = form.0;
                    let offset = unwrap!(FixedOffset::east_opt(timezone));
                    let wake_up = wake_up + offset; // Convert to UTC
                    async move {
                        actions.send(Action::SetWakeUpTime(wake_up)).await;
                        events.send(Event::WakeUpTime(wake_up)).await;
                        StatusCode::NO_CONTENT
                    }
                }),
            )
            .route(
                "/network",
                routing::post(move |form: Form<NetworkForm>| {
                    debug!("{}", &form.0);
                    let NetworkForm { wifi_ssid, wifi_password } = form.0;
                    async move {
                        events.send(Event::Network { wifi_ssid, wifi_password }).await;
                        StatusCode::NO_CONTENT
                    }
                }),
            )
            .route(
                "/state",
                routing::get(move || {
                    debug!("Querying state");
                    let state = state::get();
                    async move { Json(state).into_response() }
                }),
            )
            .route(
                "/restart",
                routing::post(move || {
                    debug!("Asked to restart");
                    async move {
                        actions.send(Action::Restart).await;
                        StatusCode::NO_CONTENT
                    }
                }),
            )
            .route(
                "/reset",
                routing::post(move || {
                    debug!("Asked to reset");
                    async move {
                        events.send(Event::Reset).await;
                        StatusCode::NO_CONTENT
                    }
                }),
            )
    }
}

pub struct WebServer {
    stack: Stack<'static>,
    config: Config<Duration>,
    router: &'static AppRouter<AppBuilderImpl>,
}

impl WebServer {
    pub fn new(
        stack: Stack<'static>,
        actions: DynamicSender<'static, Action>, // Action channel
        events: DynamicSender<'static, Event>,   // Event channel
    ) -> Self {
        let config = Config::new(Timeouts {
            start_read_request: Some(Duration::from_secs(5)),
            read_request: Some(Duration::from_secs(1)),
            write: Some(Duration::from_secs(1)),
        })
        .keep_connection_alive();
        let router = heap::make_static_with(|| AppBuilderImpl { actions, events }.build_app());
        Self { stack, config, router }
    }

    pub async fn run(
        &mut self, id: usize, tcp_rx_buffer: &mut [u8], tcp_tx_buffer: &mut [u8], http_buffer: &mut [u8],
    ) -> ! {
        listen_and_serve(id, self.router, &self.config, self.stack, 80, tcp_rx_buffer, tcp_tx_buffer, http_buffer).await
    }
}
