use chrono::{NaiveDateTime, NaiveTime};
use defmt::{unwrap, Format};
use ds323x::{ic::DS3231, interface::I2cInterface, Alarm2Matching, DateTimeAccess, DayAlarm2, Ds323x, Hours};
use esp_hal::{
    gpio::AnyPin,
    i2c::master::{self as i2c, AnyI2c, I2c},
    Blocking,
};

/// The DS3231 maintains timekeeping accuracy of ±2ppm, which is approximately 173 milliseconds per day and
/// a bit more than 1 minute per year. Adjusting it once a month with NTP should be more than enough for our purposes.
type Ds3231<'a> = Ds323x<I2cInterface<I2c<'a, Blocking>>, DS3231>;

#[derive(Format)]
pub struct Clock {
    // External RTC device
    rtc: Ds3231<'static>,
}

impl Clock {
    pub fn new(i2c: AnyI2c, sda: AnyPin, scl: AnyPin) -> Self {
        let mut rtc = {
            let device = unwrap!(I2c::new(i2c, i2c::Config::default())).with_sda(sda).with_scl(scl);
            Ds323x::new_ds3231(device)
        };
        unwrap!(rtc.enable());
        unwrap!(rtc.disable_square_wave());
        unwrap!(rtc.disable_32khz_output());
        unwrap!(rtc.use_int_sqw_output_as_interrupt());
        unwrap!(rtc.enable_alarm1_interrupts());
        unwrap!(rtc.enable_alarm2_interrupts());
        Self { rtc }
    }

    pub fn current_time(&mut self) -> NaiveDateTime {
        unwrap!(self.rtc.datetime())
    }

    pub fn set_current_time(&mut self, dt: NaiveDateTime) {
        unwrap!(self.rtc.set_datetime(&dt));
    }

    pub fn set_daily_alarm(&mut self, hour: u32, minute: u32) {
        let when = unwrap!(NaiveTime::from_hms_opt(hour, minute, 0));
        unwrap!(self.rtc.set_alarm1_hms(when));
    }

    pub fn set_monthly_alarm(&mut self, day: u8, hour: u8, minute: u8) {
        let when = DayAlarm2 { day, hour: Hours::H24(hour), minute };
        unwrap!(self.rtc.set_alarm2_day(when, Alarm2Matching::AllMatch));
    }

    pub fn take_daily_alarm(&mut self) -> bool {
        if unwrap!(self.rtc.has_alarm1_matched()) {
            unwrap!(self.rtc.clear_alarm1_matched_flag());
            true
        } else {
            false
        }
    }

    pub fn take_monthly_alarm(&mut self) -> bool {
        if unwrap!(self.rtc.has_alarm2_matched()) {
            unwrap!(self.rtc.clear_alarm2_matched_flag());
            true
        } else {
            false
        }
    }
}
