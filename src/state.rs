use core::ptr;
use defmt::{assert, Format};
use esp_alloc::HEAP;
use esp_hal::{ram, rtc_cntl::Rtc};
use portable_atomic::{AtomicPtr, AtomicU32, Ordering};
use serde::Serialize;

#[ram(rtc_slow, persistent)]
static BOOT_COUNT: AtomicU32 = AtomicU32::new(0);

#[ram(rtc_slow, persistent)]
static PANIC_COUNT: AtomicU32 = AtomicU32::new(0);

#[no_mangle]
#[allow(unused)]
fn custom_pre_backtrace() {
    PANIC_COUNT.fetch_add(1, Ordering::Relaxed);
}

#[derive(Format, Serialize)]
#[non_exhaustive]
pub struct State {
    pub heap_used: usize,
    pub heap_free: usize,
    pub boot_count: u32,
    pub panic_count: u32,
    pub uptime: u64,
    pub timestamp: i64,
}

static RTC: AtomicPtr<Rtc<'static>> = AtomicPtr::new(ptr::null_mut());

pub fn init(rtc: &'static Rtc<'static>) -> bool {
    let ptr = rtc as *const _ as *mut _;
    RTC.store(ptr, Ordering::Relaxed);
    BOOT_COUNT.fetch_add(1, Ordering::Relaxed) == 0
}

fn get_rtc() -> &'static Rtc<'static> {
    let ptr = RTC.load(Ordering::Relaxed);
    assert!(!ptr.is_null());
    unsafe { &*ptr }
}

pub fn get() -> State {
    let rtc = get_rtc();
    State {
        heap_used: HEAP.used(),
        heap_free: HEAP.free(),
        boot_count: BOOT_COUNT.load(Ordering::Relaxed),
        panic_count: PANIC_COUNT.load(Ordering::Relaxed),
        uptime: rtc.time_since_boot().to_secs(),
        timestamp: rtc.current_time().and_utc().timestamp_millis(),
    }
}
