//! SNTPv4 client

use crate::heap;
use chrono::{DateTime, NaiveDate, TimeDelta, Utc};
use core::mem;
use defmt::{assert_eq, unwrap};
use embassy_net::{
    udp::{BindError, PacketMetadata, RecvError, SendError, UdpSocket},
    IpEndpoint, IpListenEndpoint, Stack,
};
use esp_hal::rtc_cntl::Rtc;
use zerocopy::byteorder::network_endian::U32;
use zerocopy::{AsBytes, FromBytes, FromZeroes};

#[derive(Clone, Copy, AsBytes, FromBytes, FromZeroes)]
#[repr(C)]
struct Timestamp {
    seconds: U32,
    fraction: U32,
}

impl Timestamp {
    const ZERO: Self = Self { seconds: U32::ZERO, fraction: U32::ZERO };
}

/// The NTP epoch (1900-01-01 00:00:00)
pub const EPOCH: DateTime<Utc> = NaiveDate::from_ymd_opt(1900, 1, 1).unwrap().and_hms_opt(0, 0, 0).unwrap().and_utc();

impl From<Timestamp> for DateTime<Utc> {
    fn from(ts: Timestamp) -> Self {
        let nanos: u64 = (u64::from(ts.fraction.get()) * 1_000_000_000) >> 32;
        let nanos: u32 = unwrap!(nanos.try_into());
        let secs = ts.seconds.get().into();
        let delta = unwrap!(TimeDelta::new(secs, nanos));
        EPOCH + delta
    }
}

impl From<DateTime<Utc>> for Timestamp {
    fn from(dt: DateTime<Utc>) -> Self {
        let delta = dt - EPOCH;
        let seconds = unwrap!(delta.num_seconds().try_into());
        let nanos: u64 = unwrap!(delta.subsec_nanos().try_into());
        let fraction = (nanos << 32) / 1_000_000_000;
        let fraction: u32 = unwrap!(fraction.try_into());
        Timestamp { seconds: U32::new(seconds), fraction: U32::new(fraction) }
    }
}

#[derive(AsBytes, FromBytes, FromZeroes)]
#[repr(C)]
struct Message {
    li_vn_mode: u8,
    stratum: u8,
    poll: i8,
    precision: i8,
    root_delay: U32,
    root_dispersion: U32,
    reference_id: U32,
    reference_timestamp: Timestamp,
    originate_timestamp: Timestamp,
    receive_timestamp: Timestamp,
    transmit_timestamp: Timestamp,
}

const SIZE: usize = mem::size_of::<Message>().next_power_of_two();

struct NtpState {
    rx_meta: [PacketMetadata; 1],
    rx_buffer: [u8; SIZE],
    tx_meta: [PacketMetadata; 1],
    tx_buffer: [u8; SIZE],
}

impl Default for NtpState {
    fn default() -> Self {
        Self::new()
    }
}

impl NtpState {
    pub fn new() -> Self {
        Self {
            rx_meta: [PacketMetadata::EMPTY; 1],
            rx_buffer: [0; SIZE],
            tx_meta: [PacketMetadata::EMPTY; 1],
            tx_buffer: [0; SIZE],
        }
    }
}

pub struct NtpClient {
    socket: UdpSocket<'static>,
    rtc: &'static Rtc<'static>,
}

impl NtpClient {
    pub fn new(stack: Stack<'static>, rtc: &'static Rtc<'static>) -> Result<Self, BindError> {
        let state = heap::make_static_with(NtpState::new);
        let mut socket =
            UdpSocket::new(stack, &mut state.rx_meta, &mut state.rx_buffer, &mut state.tx_meta, &mut state.tx_buffer);
        socket.bind(IpListenEndpoint { addr: None, port: 123 })?;
        Ok(Self { socket, rtc })
    }

    pub async fn query(&mut self, origin: DateTime<Utc>, server: IpEndpoint) -> Option<DateTime<Utc>> {
        let before = self.rtc.time_since_boot();

        let (receive_timestamp, transmit_timestamp) = {
            self.send(origin.into(), server).await.ok()?;
            self.recv().await.ok()?
        };

        let after = self.rtc.time_since_boot();

        let delta = unwrap!(after.checked_sub(before));
        let delta = TimeDelta::nanoseconds(unwrap!(delta.to_nanos().try_into()));

        let t1 = origin;
        let t2 = DateTime::from(receive_timestamp);
        let t3 = DateTime::from(transmit_timestamp);
        let t4 = origin + delta;

        let _delay = (t4 - t1) - (t3 - t2);
        let offset = ((t2 - t1) + (t3 - t4)) / 2;
        Some(origin + offset)
    }

    async fn send(&mut self, ts: Timestamp, server: IpEndpoint) -> Result<(), SendError> {
        let message = Message {
            li_vn_mode: 0b00100011, // LI=0, VN=4, Mode=3
            stratum: 0,
            poll: 0,
            precision: 0,
            root_delay: U32::ZERO,
            root_dispersion: U32::ZERO,
            reference_id: U32::ZERO,
            reference_timestamp: Timestamp::ZERO,
            originate_timestamp: Timestamp::ZERO,
            receive_timestamp: Timestamp::ZERO,
            transmit_timestamp: ts,
        };
        self.socket.send_to(message.as_bytes(), server).await
    }

    async fn recv(&mut self) -> Result<(Timestamp, Timestamp), RecvError> {
        let mut message = Message::new_zeroed();
        let (len, _) = self.socket.recv_from(message.as_bytes_mut()).await?;
        if len == mem::size_of_val(&message) {
            // TODO more checks
            assert_eq!(message.li_vn_mode & 0b00111111, 0b00100100); // LI=?, VN=4, Mode=4
            Ok((message.receive_timestamp, message.transmit_timestamp))
        } else {
            Err(RecvError::Truncated)
        }
    }
}
