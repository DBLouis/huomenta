//! # Never
//!
//! This module provides a safe way to extend the lifetime of a reference to `'static`, inside a
//! function that never returns (e.g. `fn main() -> !`).
//!
//! ## Example
//!
//! ```rust
//! use never::never;
//!
//! fn main() -> ! {
//!    never::call(|scope| {
//!      let mut value = 42_i32;
//!      never!(scope, value);
//!      // `value` is now of type `&'static mut i32`
//!      loop {
//!        value.wrapping_add(1);
//!      }
//!    })
//! }
//! ```
use core::{cell::Cell, marker::PhantomData};
use defmt::{unreachable, Format};

mod scope {
    use super::{NeverCell, Seized};
    use core::marker::PhantomData;
    use defmt::Format;

    #[derive(Format)]
    pub struct NeverScope {
        // Only constructible from within this module.
        _not_send: PhantomData<*mut ()>,
    }

    impl NeverScope {
        /// Creates a new `NeverScope`, implying that the calling function will never return.
        ///
        /// # Safety
        ///
        /// A `NeverScope` must *only* be constructed in a function that will never return.
        #[inline(always)]
        pub(super) const unsafe fn assume_never() -> Self {
            // SAFETY: Caller promised that the calling function will never return.
            Self { _not_send: PhantomData }
        }

        /// Extends the lifetime of a reference to `'static`.
        #[inline(always)]
        pub fn forever<T>(&self, x: &mut NeverCell<Seized, T>) -> &'static mut T {
            // SAFETY: A `NeverCell<Seized, T>` can only be created once for a given variable,
            // therefore `x` is a unique mutable reference to a `T`, and it is safe to extend its
            // lifetime to `'static` because of the existence of `NeverScope`.
            unsafe { x.as_static() }
        }
    }

    impl Drop for NeverScope {
        fn drop(&mut self) {
            // By definition, a `NeverScope` must never be dropped because it is only created in
            // functions that never return.
            unreachable!()
        }
    }
}
pub use scope::NeverScope;

/// A marker type that indicates that a [NeverCell] has been seized, meaning that a mutable
/// reference to it has been obtained and the original binding is not accessible anymore (shadowed).
/// In other words, a `&mut NeverCell<Seized, T>` can only be created once for a given variable,
/// even if that reference ends up being dropped, it cannot be created again. This cannot be
/// enforced in the type system, but it is a safety invariant that must be upheld when calling
/// [`NeverCell::seize`].
#[derive(Format)]
pub struct Seized;

#[derive(Format)]
#[repr(transparent)]
pub struct NeverCell<S, T> {
    value: T,
    state: PhantomData<S>,
    _not_sync: PhantomData<Cell<()>>,
}

impl<T> NeverCell<(), T> {
    /// Creates a new `NeverCell<(), T>` with the given value.
    pub const fn new(value: T) -> Self {
        Self { value, state: PhantomData, _not_sync: PhantomData }
    }

    /// Converts a `&mut NeverCell<(), T>` into a `&mut NeverCell<Seized, T>`.
    ///
    /// # Safety
    ///
    /// The caller must guarantee that the variable referred by `&mut self` cannot be accessed
    /// again. It can be achieved by shadowing the original binding with the return value of this
    /// function, like so:
    /// ```rust
    /// let mut value = 42_i32;
    /// let value = NeverCell::new(value);
    /// let value: &mut NeverCell<Seized, _> = unsafe { value.seize() };
    /// // Now we have a unique mutable reference to `value` and the original binding is shadowed,
    /// // so even if we drop the reference, we can't call `seize` again.
    /// ```
    pub const unsafe fn seize(&mut self) -> &'_ mut NeverCell<Seized, T> {
        // SAFETY: The caller ensures that this function is called once for a given variable.
        // SAFETY: The memory layout of `NeverCell<Seized, T>` is the same as `NeverCell<(), T>`.
        unsafe { core::mem::transmute(self) }
    }
}

impl<T> NeverCell<Seized, T> {
    /// Converts a `NeverCell<Seized, T>` into a `&'static mut T`.
    ///
    /// # Safety
    ///
    /// The output reference has the `'static` lifetime, so it is not bound by the input lifetime.
    /// This function must never be called more than once for a given `NeverCell<Seized, T>`.
    unsafe fn as_static(&mut self) -> &'static mut T {
        // SAFETY: Memory layouts are the same.
        unsafe { core::mem::transmute(self) }
    }
}

impl<S, T> Drop for NeverCell<S, T> {
    fn drop(&mut self) {
        unreachable!()
    }
}

/// Calls the given closure with a reference to a [`NeverScope`]. The closure must never return.
/// Inside the closure, you can use the [`never!`](crate::never!) macro to obtain `'static` references to local
/// variables.
#[inline]
pub fn call(f: impl FnOnce(&NeverScope) -> !) -> ! {
    // SAFETY: The closure `f` never returns.
    let scope = unsafe { NeverScope::assume_never() };
    f(&scope)
}

/// This macro can be used to extend the lifetime of a reference to `'static` inside a function
/// that never returns. You need to pass a reference to a [NeverScope] which can only be obtained
/// in a function that never returns.
#[macro_export]
macro_rules! never {
    ($scope:ident, $variable:ident) => {
        // Move value to ensure that it is owned.
        let mut $variable = $crate::never::NeverCell::new($variable);
        // Shadow the original binding so that it can't be used again.
        let $variable = unsafe { $variable.seize() };
        // Now the original binding is inaccessible, and we have a unique mutable reference to it.
        let $variable: &'static mut _ = $scope.forever($variable);
    };
}
