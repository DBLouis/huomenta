use crate::heap;
use defmt::{assert, debug, unwrap};
use embedded_hal::pwm::SetDutyCycle;
use esp_hal::{
    gpio::{AnyPin, Level, Output},
    ledc::{
        channel::{self, Channel, ChannelIFace},
        timer::{self, TimerIFace},
        LSGlobalClkSource, Ledc, LowSpeed,
    },
    time::RateExtU32,
};

pub struct Lamp {
    channel: Channel<'static, LowSpeed>,
    psu: Output<'static>,
}

impl Lamp {
    pub fn new(ledc: &'static mut Ledc<'static>, pwm_pin: AnyPin, psu_pin: AnyPin) -> Self {
        ledc.set_global_slow_clock(LSGlobalClkSource::APBClk);

        let lst = heap::make_static_with(|| ledc.timer::<LowSpeed>(timer::Number::Timer0));
        unwrap!(lst.configure(timer::config::Config {
            duty: timer::config::Duty::Duty10Bit,
            clock_source: timer::LSClockSource::APBClk,
            frequency: 24.kHz(),
        }));

        let mut ch0 = ledc.channel(channel::Number::Channel0, pwm_pin);
        unwrap!(ch0.configure(channel::config::Config {
            timer: lst,
            duty_pct: 0,
            pin_config: channel::config::PinConfig::PushPull,
        }));

        let psu = Output::new(psu_pin, Level::Low);
        Self { channel: ch0, psu }
    }

    pub fn set_brightness(&mut self, value: f32) {
        const GAMMA: f32 = 2.2;
        const SCALE: f32 = 0.8;

        assert!((0.0..=1.0).contains(&value));
        let value = value * SCALE; // Scale to 80% maximum brightness

        let max = self.channel.max_duty_cycle() as f32;
        let value = (libm::powf(value, GAMMA) * max) as u16;
        debug!("Setting duty cycle to {}", value);
        if value == 0 {
            debug!("Disabling PSU");
            self.psu.set_low();
        } else {
            debug!("Enabling PSU");
            self.psu.set_high();
        }
        unwrap!(self.channel.set_duty_cycle(value))
    }
}
