console.log('Huomenta!');

document.getElementById('inputBrightness').addEventListener('input', (event) => {
  document.getElementById('brightness').innerText = `Brightness: ${event.target.value}%`;
});

document.getElementById('buttonReset').addEventListener('click', () => {
  fetch('/reset', { method: 'POST' });
});

document.getElementById('buttonRestart').addEventListener('click', () => {
  fetch('/restart', { method: 'POST' });
});

document.getElementById('inputTimezone').value = new Date().getTimezoneOffset() * 60;

function fetchState() {
  fetch('/state').then(response => response.json()).then(state => {
    const date = new Date(state.timestamp);
    document.getElementById('date').innerText = date.toLocaleString();
    const hours = Math.floor(state.uptime / 3600).toString().padStart(2, '0');
    const minutes = Math.floor(state.uptime % 3600 / 60).toString().padStart(2, '0');
    const seconds = Math.floor(state.uptime % 60).toString().padStart(2, '0');
    document.getElementById('uptime').innerText = `Uptime: ${hours}:${minutes}:${seconds}`;
    const heap_used = Math.floor(state.heap_used / 1024);
    const heap_total = Math.floor((state.heap_used + state.heap_free) / 1024);
    document.getElementById('heap').innerText = `Heap: ${heap_used} / ${heap_total} KiB`;
    document.getElementById('boot').innerText = `Boot count: ${state.boot_count}`;
    document.getElementById('panic').innerText = `Panic count: ${state.panic_count}`;
  });
}

fetchState();
