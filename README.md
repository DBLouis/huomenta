# Huomenta! Light Wake-Up Alarm Clock

## What is this?

A light wake-up alarm clock that uses an ESP32 microcontroller to drive a LED strip.
The light intensity increases gradually to simulate a sunrise.
It comes with a web interface to change its settings or control it manually.

---

*Huomenta means good morning in Finnish.*
